FROM vsiri/gbdx_dockers:vxl_python


LABEL dustify.runargs="-v %PWD%/.gbdx-config:/root/.gbdx-config"

ADD gbdx_entrypoint.bsh /
ADD register.py inspect_gbdx.py launch.py unregister.py /

ENTRYPOINT ["/gbdx_entrypoint.bsh"]

CMD ["launch"]
