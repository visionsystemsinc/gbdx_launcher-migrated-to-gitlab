#!/usr/bin/env python

import gbdxtools
from pprint import pprint
import logging
import argparse
import time
import datetime

def parser():
  my_parser = argparse.ArgumentParser()
  aa = my_parser.add_argument
  aa('-v', dest='verbose', default=False, action='store_true', help='Verbose')
  return my_parser

def current_time():
  return (datetime.datetime.now()).strftime("%Y-%m-%d %H:%M")

class Menu(object):
  def __init__(self, gbdx):
    self.gbdx = gbdx

  def menu(self):
    run = True
    while run:
      print '1) Workflows'
      print '2) Tasks'
      print
      print 'q) Quit'
      i = raw_input()
      if i=='1':
        self.workflows()
      elif i=='2':
        self.tasks()
      elif i.lower()=='q':
        run=False

  def workflows(self):
    while True:
      workflows = self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows').json()['Workflows']
      if workflows:
        print '0) Custom'
        for index, workflow_id in enumerate(workflows):
          print '%d) %s' % (index+1, workflow_id)

        workflow = raw_input()
        try:
          workflow = int(workflow)-1
          if workflow == -1:
            self.workflow_info(raw_input())
          else:
            self.workflow_info(workflows[workflow])
        except:
          return

  def workflow_info(self, workflow_id):
    while True:
      print 'Workflow %s' % workflow_id
      print '--------------------------'
      print '1) Status'
      print '2) Stdout'
      print '3) Stderr'
      print '4) Cancel Workflow'
      print '5) Watch'

      i = raw_input()
      try:
        i = int(i)
      except:
        return

      info = self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows/' + workflow_id).json()
      task_ids = [x['id'] for x in info['tasks']]

      if i==1:
        pprint(info)
      elif i>=2 and i<= 3:
        while 1:
          for index, task_id in enumerate(task_ids):
            print '%d) %s' % (index+1, task_id)
            
          task = raw_input()
          try:
            task = int(task)-1
          except:
            break

          task_id = task_ids[task]

          if i==2:
            output = self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows/' + workflow_id+'/tasks/'+task_id+'/stdout').text
          elif i==3:
            output = self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows/' + workflow_id+'/tasks/'+task_id+'/stderr').text
          print output
      elif i==4:
        self.gbdx.workflow.cancel(workflow_id)
      elif i==5:
        self.watch(workflow_id)
      else:
        return

  def watch(self, workflow_id):
    while True:
      info = self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows/' + workflow_id).json()
      print info['state']['state'], '--', current_time()
      if info['state']['state'] != 'pending':
        break
      time.sleep(60)
    
    task_ids = [x['id'] for x in info['tasks']]

    for task_id in task_ids:
      print self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows/' + workflow_id+'/tasks/'+task_id+'/stdout').text
      print self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/workflows/' + workflow_id+'/tasks/'+task_id+'/stderr').text

  def tasks(self):
    task_list = gbdxtools.task_registry.TaskRegistry().list()
#     task_list = gbdxtools.workflow.Workflow(self.gbdx).list_tasks()['tasks']
    for index, task_name in enumerate(task_list):
      print '%d) %s' % (index+1, task_name)

    task = raw_input()
    try:
      task = int(task)-1
    except:
      return

    info = self.gbdx.gbdx_connection.get('https://geobigdata.io/workflows/v1/tasks/' + task_list[task])
    pprint(info.json())

def main(args=None):
  args = parser().parse_args(args)
  if args.verbose:
    try: # for Python 3
      from http.client import HTTPConnection
    except ImportError:
      from httplib import HTTPConnection

    HTTPConnection.debuglevel = 1

    logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

  gbdx = gbdxtools.Interface()

  menu = Menu(gbdx)
  menu.menu();

if __name__=="__main__":
  main()
