#!/usr/bin/env python

import gbdxtools
import argparse
import logging

tasks = {
         "VSI_dem_master":{
            "inputPortDescriptors":[
                {
                    "description":"GBDX username",
                    "required":True,
                    "name":"username",
                    "type":"string"
                },
                {
                    "description":"GBDX password",
                    "required":True,
                    "name":"password",
                    "type":"string"
                },
                {
                    "description":"GBDX client id",
                    "required":True,
                    "name":"client_id",
                    "type":"string"
                },
                {
                    "description":"GBDX client secret",
                    "required":True,
                    "name":"client_secret",
                    "type":"string"
                },
                {
                    "description":"S3 location holding shapefile input data.",
                    "required":True,
                    "name":"s3_shapefiles",
                    "type":"string"
                },
                {
                    "description":"Start year of the desired images",
                    "required":True,
                    "name":"start_year",
                    "type":"string"
                },
                {
                    "description":"End year of the desired images",
                    "required":True,
                    "name":"end_year",
                    "type":"string"
                },
                {
                    "description":"S3 location holding all outputs data.",
                    "required":True,
                    "name":"s3_outprefix",
                    "type":"string"
                },
                {
                    "description":"S3 location holding ASTER DEM images.",
                    "required":True,
                    "name":"s3_demprefix",
                    "type":"string"
                },
                {
                    "description":"batch size of batch WorkFlow (maximum batch size is 36)",
                    "required":False,
                    "name":"batch_size",
                    "type":"string"
                },
                {
                    "description":"number of small scene tiles used for geo-correction",
                    "required":False,
                    "name":"num_scenes",
                    "type":"string"
                },
                {
                    "description":"option to turn on image catalog and satellite resource creation tasks, allowed inputs are True/true/False/false, default is ON",
                    "required":False,
                    "name":"is_sat_res_creation",
                    "type":"string"
                },
                {
                    "description":"option to turn on image geo-correction, allowed inputs are True/true/False/false, default is ON",
                    "required":False,
                    "name":"is_geocorrection",
                    "type":"string"
                },
                {
                    "description":"option to turn on region DEM generation, allowed inputs are True/true/False/false, default is ON",
                    "required":False,
                    "name":"is_dem_generation",
                    "type":"string"
                }
            ],
            "outputPortDescriptors":[],
            "containerDescriptors":[
                {
                    "type":"DOCKER",
                    "command":"",
                    "properties":
                    {
                        "image":"vsiri/gbdx_dockers:dem_generate_master"
                    }
                }
            ],
            "description":"Master supertask to launch VSI's high resolution DEM generation",
            "properties":{
                "timeout":172800,
                "authorizationRequired": False,
                "isPublic":True
            },
            "version": "0.0.1"
        },
         "VSI_dem_catalog":{
            "inputPortDescriptors":[
                {
                    "description":"GBDX username",
                    "required":True,
                    "name":"username",
                    "type":"string"
                },
                {
                    "description":"GBDX password",
                    "required":True,
                    "name":"password",
                    "type":"string"
                },
                {
                    "description":"GBDX client id",
                    "required":True,
                    "name":"client_id",
                    "type":"string"
                },
                {
                    "description":"GBDX client secret",
                    "required":True,
                    "name":"client_secret",
                    "type":"string"
                },
                {
                    "description":"S3 location holding shapefile input data.",
                    "required":True,
                    "name":"s3_shapefiles",
                    "type":"string"
                },
                {
                    "description":"S3 location holding all outputs data.",
                    "required":True,
                    "name":"s3_outprefix",
                    "type":"string"
                },
                {
                    "description":"Start year of the desired images",
                    "required":True,
                    "name":"start_year",
                    "type":"string"
                },
                {
                    "description":"End year of the desired images",
                    "required":True,
                    "name":"end_year",
                    "type":"string"
                 }
            ],
            "outputPortDescriptors":[
                {
                    "description":"multiple manifest files of ordered images",
                    "required":True,
                    "name":"manifest_file",
                    "type":"string"
                }
            ],
            "containerDescriptors":[
                {
                    "type":"DOCKER",
                    "command":"",
                    "properties":
                    {
                        "image":"vsiri/gbdx_dockers:dem_catalog"
                    }
                }
            ],
            "description":"Runs catalog and ordering tasks to order data for processing",
            "properties":{
                "timeout":172800,
                "authorizationRequired": False,
                "isPublic":True
            },
            "name":"VSI_dem_catalog",
            "version": "0.0.1"
        },
        "VSI_dem_manifest_process":{
           "inputPortDescriptors":[
                {
                    "description":"GBDX username",
                    "required":True,
                    "name":"username",
                    "type":"string"
                },
                {
                    "description":"GBDX password",
                    "required":True,
                    "name":"password",
                    "type":"string"
                },
                {
                    "description":"GBDX client id",
                    "required":True,
                    "name":"client_id",
                    "type":"string"
                },
                {
                    "description":"GBDX client secret",
                    "required":True,
                    "name":"client_secret",
                    "type":"string"
                },
                {
                    "description":"Multiple image manifest files in S3 receiving bucket",
                    "required":True,
                    "multiplex":True,
                    "name":"manifest_files",
                    "type":"directory"
                },
                {
                    "description":"S3 location holding all outputs data.",
                    "required":True,
                    "name":"s3_outprefix",
                    "type":"string"
                }
            ],
            "outputPortDescriptors":[],
            "containerDescriptors":[
                {
                    "type":"DOCKER",
                    "command":"",
                    "properties":
                    {
                        "image":"vsiri/gbdx_dockers:dem_manifest_process"
                    }
                }
            ],
            "description":"Create satellite image database based on input S3 location of images",
            "properties":{
                "timeout":172800,
                "authorizationRequired": False,
                "isPublic":True
            },
            "name":"VSI_dem_manifest_process",
            "version": "0.0.1"
         },
        "VSI_dem_sat_res_creation":{
            "inputPortDescriptors":[
                {
                    "description":"GBDX username",
                    "required":True,
                    "name":"username",
                    "type":"string"
                },
                {
                    "description":"GBDX password",
                    "required":True,
                    "name":"password",
                    "type":"string"
                },
                {
                    "description":"GBDX client id",
                    "required":True,
                    "name":"client_id",
                    "type":"string"
                },
                {
                    "description":"GBDX client secret",
                    "required":True,
                    "name":"client_secret",
                    "type":"string"
                },
                {
                    "description":"S3 location holding shapefile input data.",
                    "required":True,
                    "name":"s3_shapefiles",
                    "type":"string"
                },
                {
                    "description":"image metadata json on S3 customer bucket",
                    "required":True,
                    "name":"s3_meta_dict",
                    "type":"string"
                },
                {
                    "description":"Multiple S3 receiving bucket locations",
                    "required":True,
                    "multiplex":True,
                    "name":"metadata_files",
                    "type":"directory"
                },
                {
                    "description":"S3 location holding all outputs data.",
                    "required":True,
                    "name":"s3_outprefix",
                    "type":"string"
                }
            ],
            "outputPortDescriptors":[],
            "containerDescriptors":[
                {
                    "type":"DOCKER",
                    "command":"",
                    "properties":
                    {
                        "image":"vsiri/gbdx_dockers:dem_sat_res_creation"
                    }
                }
            ],
            "description":"Create satellite image database based on input S3 location of images",
            "properties":{
                "timeout":172800,
                "authorizationRequired": False,
                "isPublic":True
            },
            "name":"VSI_dem_sat_res_creation",
            "version": "0.0.1"
        },
        "VSI_dem_geocorrect_worker":{
            "inputPortDescriptors":[
                {
                    "description":"GBDX username",
                    "required":True,
                    "name":"username",
                    "type":"string"
                },
                {
                    "description":"GBDX password",
                    "required":True,
                    "name":"password",
                    "type":"string"
                },
                {
                    "description":"GBDX client id",
                    "required":True,
                    "name":"client_id",
                    "type":"string"
                },
                {
                    "description":"GBDX client secret",
                    "required":True,
                    "name":"client_secret",
                    "type":"string"
                },
                {
                    "description":"S3 location holding all outputs data.",
                    "required":True,
                    "name":"s3_outprefix",
                    "type":"string"
                },
                {
                    "description":"S3 location holding ASTER DEM images.",
                    "required":True,
                    "name":"s3_demprefix",
                    "type":"string"
                },
                {
                    "description":"S3 location holding shapefile input data.",
                    "required":True,
                    "name":"s3_shapefiles",
                    "type":"string"
                },
                {
                    "description":"S3 location holding file specfying scene ids that task will operates",
                    "required":True,
                    "name":"scene_list_txt",
                    "type":"string"
                },
                {
                    "description":"Satellite images that will be used",
                    "required":True,
                    "multiplex":True,
                    "name":"sat_img",
                    "type":"directory"
                }
            ],
            "outputPortDescriptors":[],
            "containerDescriptors":[
                {
                    "type":"DOCKER",
                    "command":"",
                    "properties":
                    {
                        "image":"vsiri/gbdx_dockers:dem_geocorrect_worker"
                    }
                }
            ],
            "description":"Runs image feature matching on 1B products",
            "properties":{
                "timeout":172800,
                "authorizationRequired": False,
                "isPublic":True
            },
            "name":"VSI_dem_geocorrect_worker",
            "version": "0.0.1"
        },
        "VSI_dem_generate_worker":{
            "inputPortDescriptors":[
                {
                    "description":"GBDX username",
                    "required":True,
                    "name":"username",
                    "type":"string"
                },
                {
                    "description":"GBDX password",
                    "required":True,
                    "name":"password",
                    "type":"string"
                },
                {
                    "description":"GBDX client id",
                    "required":True,
                    "name":"client_id",
                    "type":"string"
                },
                {
                    "description":"GBDX client secret",
                    "required":True,
                    "name":"client_secret",
                    "type":"string"
                },
                {
                    "description":"S3 location holding all outputs data.",
                    "required":True,
                    "name":"s3_outprefix",
                    "type":"string"
                },
                {
                    "description":"S3 location holding ASTER DEM images.",
                    "required":True,
                    "name":"s3_demprefix",
                    "type":"string"
                },
                {
                    "description":"S3 location holding shapefile input data.",
                    "required":True,
                    "name":"s3_shapefiles",
                    "type":"string"
                },
                {
                    "description":"S3 location holding file specfying scene ids that task will operates",
                    "required":True,
                    "name":"scene_list_txt",
                    "type":"string"
                },
                {
                    "description":"S3 location holding file geo-corrected cameras",
                    "required":True,
                    "name":"s3_cor_camprefix",
                    "type":"string"
                },
                {
                    "description":"Satellite images that will be used",
                    "required":True,
                    "multiplex":True,
                    "name":"sat_img",
                    "type":"directory"
                }
            ],
            "outputPortDescriptors":[],
            "containerDescriptors":[
                {
                    "type":"DOCKER",
                    "command":"",
                    "properties":
                    {
                        "image":"vsiri/gbdx_dockers:dem_generate_worker"
                    }
                }
            ],
            "description":"Runs image feature matching on 1B products",
            "properties":{
                "timeout":172800,
                "authorizationRequired": False,
                "isPublic":True
            },
            "name":"VSI_dem_generate_worker",
            "version": "0.0.1"
        }
}

def register_task(gbdx, task_name):
  task = tasks[task_name]
  task['name'] = task_name

  print 'Registering', task_name
  gbdx.gbdx_connection.post('https://geobigdata.io/workflows/v1/tasks', json=task)
  print 'Registered', task_name


def parser():
  my_parser = argparse.ArgumentParser()
  aa = my_parser.add_argument
  aa('-v', dest='verbose', default=False, action='store_true', help='Verbose')
  return my_parser

def main(args=None):
  args = parser().parse_args(args)
  if args.verbose:
    try: # for Python 3
      from http.client import HTTPConnection
    except ImportError:
      from httplib import HTTPConnection

    HTTPConnection.debuglevel = 1

    logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
  
  gbdx = gbdxtools.Interface()

  tr = gbdxtools.task_registry.TaskRegistry()
  task_list = tr.list()

  for task_name, task_json in tasks.iteritems():
    task_name_with_version = "%s:%s" % (str(task_name), str(task_json["version"]))
    if task_name_with_version not in task_list:
      register_task(gbdx, task_name)
    else:
      print task_name, 'is already registered'

if __name__=="__main__":
  main()
