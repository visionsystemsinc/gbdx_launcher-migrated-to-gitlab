#!/usr/bin/env python

import gbdxtools
from configparser import ConfigParser
import os, sys, time, pprint, random
import json
import logging
import argparse
import uuid

def query_yes_no(question, default="yes"):
  '''Ask a yes/no question via raw_input() and return their answer.
  @type:  question, string
  @param: question, string that is presented to the user.
  @type:  default, string
  @param  default, the presumed answer if the user just hits <Enter>. It must be "yes" (the default), "no" or None (meaning an answer is required of the user).
  
  @rtype  boolean
  :return The "answer" return value is True for "yes" or False for "no".
  '''
  valid = {"yes": True, "Y": True, "y": True, "ye": True, "no": False, "n": False, "N": False}
  if default is None:
    prompt = " [y/n] "
  elif default == "yes":
    prompt = " [Y/n] "
  elif default == "no":
    prompt = " [y/N] "
  else:
    raise ValueError("invalid default answer: '%s'" % default)
  while True:
    sys.stdout.write(question + prompt)
    choice = raw_input().lower()
    if default is not None and choice == '':
      return valid[default]
    elif choice in valid:
      return valid[choice]
    else:
      sys.stdout.write("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")

def launch_master(gbdx, s3_shapefiles, s3_outprefix, s3_dem_folder,
                  start_year = 2002, end_year = 2018,
                  num_scenes = 40,
                  batch_size = 4,
                  is_geocorrection = True, is_sat_res_creation = True, is_dem_generation = True):
   # retrieve gbdx credentials from gbdx-config
  config_file = "/root/.gbdx-config"
  cfg = ConfigParser()
  if not cfg.read(config_file):
    raise RuntimeError('No ini file found at {} to parse.'.format(config_file))
  ###
  # retrieve gbdx credentials from config file
  ###
  client_id     = cfg.get('gbdx', 'client_id')
  client_secret = cfg.get('gbdx', 'client_secret')
  user_name     = cfg.get('gbdx', 'user_name')
  user_password = cfg.get('gbdx', 'user_password')
  # define the WorkFlow
  dem_task_dict = {}
  dem_task_dict.update({"containerDescriptors":[{"properties":{"domain":"default"}}]})
  dem_task_dict.update({"name":"dem_generate_master"})
  dem_task_dict.update({"outputs":[]})
  dem_task_dict.update({"taskType": "VSI_dem_master"})
  dem_task_dict.update({"inputs":[]})
  dem_task_dict["inputs"].append({"name":"username",      "value":user_name})
  dem_task_dict["inputs"].append({"name":"password",      "value":user_password})
  dem_task_dict["inputs"].append({"name":"client_id",     "value":client_id})
  dem_task_dict["inputs"].append({"name":"client_secret", "value":client_secret})
  dem_task_dict["inputs"].append({"name":"s3_outprefix",  "value":s3_outprefix})
  dem_task_dict["inputs"].append({"name":"s3_shapefiles", "value":s3_shapefiles})
  dem_task_dict["inputs"].append({"name":"s3_demprefix",  "value":s3_dem_folder})
  dem_task_dict["inputs"].append({"name":"start_year",    "value":str(start_year)})
  dem_task_dict["inputs"].append({"name":"end_year",      "value":str(end_year)})
  dem_task_dict["inputs"].append({"name":"num_scenes",    "value":str(num_scenes)})
  dem_task_dict["inputs"].append({"name":"batch_size",    "value":str(batch_size)})

  if not is_geocorrection:
    dem_task_dict["inputs"].append({"name":"is_geocorrection",    "value":"False"})
  if not is_sat_res_creation:
    dem_task_dict["inputs"].append({"name":"is_sat_res_creation", "value":"False"})
  if not is_dem_generation:
    dem_task_dict["inputs"].append({"name":"is_dem_generation",   "value":"False"})

  dem_wf_payload = {}
  dem_wf_payload.update({"name":"vsi_dem_generation"})
  dem_wf_payload.update({"tasks":[dem_task_dict]})
  
  pprint.pprint(dem_wf_payload)
  if not query_yes_no("please confirm to run", "yes"):
    print "exit without running"
    return None
  # submit and launch
  sat_res_wf_id = gbdx.workflow.launch(dem_wf_payload)
  return sat_res_wf_id

def main(args=None):
  #######################
  ### input
  #######################
  s3_shapefiles = "malaysian/shp"
  s3_dem_folder = "malaysian/ASTER"
  s3_outprefix  = "malaysian/3DModel-Apr24_1"
  catalog_start_year = 1999
  catalog_end_year = 2018
  num_scenes = 40
  batch_size = 4
  is_geocorrection = True
  is_sat_res_creation = True
  is_dem_generation = True

  # Start to connect GBDX 
  gbdx = gbdxtools.Interface()
  # launch the main workflow
  print "*" * 200
  print " " * 90, "START!!!"
  print "*" * 200

  sat_res_wf_id = launch_master(gbdx, s3_shapefiles, s3_outprefix, s3_dem_folder,
                                start_year = catalog_start_year, end_year = catalog_end_year,
                                num_scenes = num_scenes,
                                batch_size = batch_size,
                                is_geocorrection=is_geocorrection, is_sat_res_creation=is_sat_res_creation, is_dem_generation = is_dem_generation)

  print "DEM generation work flow", sat_res_wf_id, "is submitted!!!"


if __name__=="__main__":
  main()
