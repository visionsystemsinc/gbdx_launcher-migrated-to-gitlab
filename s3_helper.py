'''
Created on Jul 13, 2015
various functions that interacts with AWS S3

Require: boto, filechunkio
@author: yi
'''
import gc
import boto.s3.bucket
from boto.s3.connection import S3Connection
from boto.s3.key import Key

import sys, os, glob
import random

def all_same(items):
  '''Return True if items in the list are all the same
  '''
  return all(x == items[0] for x in items)

class S3(object):
  def __init__(self, profile_name=None, aws_access_key_id=None, aws_secret_access_key=None, security_token=None):
    '''Construct given all AWS credentials
    '''
    self.profile = profile_name
    self.access_key = aws_access_key_id
    self.secret_key = aws_secret_access_key
    self.security_token = security_token
  
  def connect(self):
    '''return a S3 connection
    '''
    if self.access_key and self.secret_key:
      s3 = S3Connection(self.access_key, self.secret_key, security_token = self.security_token)
    else:
      s3 = S3Connection(profile_name = self.profile, security_token = self.security_token)
    return s3

  def get_bucket(self, bucket_name):
    '''Get a bucket from S3 account
    @type  bucket_name: string
    @param bucket_name: Bucket to check
    :rtype   a bucket object
    '''
    s3 = self.connect()
    if not self.security_token:
      bucket = s3.get_bucket(bucket_name)
    else:
      bucket = s3.get_bucket(bucket_name, validate=False, headers={'x-amz-security-token': self.security_token})
    return bucket

  def is_bucket(self, bucket_name):
    '''Check whether bucket exists
    @type  bucket_name: string
    @param bucket_name: Bucket to check
    :rtype   boolean
    :return  True if bucket exists
    '''
    s3 = self.connect()
    if self.security_token:
      if s3.lookup(bucket_name, validate=False, headers={'x-amz-security-token': self.security_token}):
        return True
      else:
        return False
    else:
      if s3.lookup(bucket_name):
        return True
      else:
        return False

  def get_all_files_in_bucket(self, bucket_name, prefix = None):
    '''Return all the files in the given bucket
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  prefix, string
    @param prefix, allows you to limit the listing to a particular prefix. E.g. if you call the method with prefix='/foo/' then the iterator will only cycle through
                   the keys that begin with the string '/foo/'.  If none is specified, all files in the S3 bucket will be returned
    :rtype  list
    :return list of files in the bucket that have given prefix (S3 file signatures.)
    '''
    bucket = self.get_bucket(bucket_name)
    key_list = []
    if prefix:
      keys = bucket.list(prefix)
    else:
      keys = bucket.list()
    for key in keys:
      key_list.append(str(key.name))
    return key_list

  def is_file_on_s3(self, bucket_name, remote_file):
    '''Return true if the given remote file name existed in S3
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  remote_file, string
    @param remote_file, filename needs to be looked for on S3
    :rtype  boolean
    :return True if file existed on S3
    '''
    files = self.get_all_files_in_bucket(bucket_name, remote_file)
    if files:
      return True
    else:
      return False

  def remove_files_in_bucekt(self, bucket_name, remove_files):
    '''Remove the given files on S3 bucket
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  remove_files, list of string
    @param remove_files, list of files that will be deleted from S3 bucket
    :rtype  integer
    :return number of files that are successfully deleted
    '''
    DeleteCount = 0
    if not remove_files:
      return DeleteCount
    if not self.is_bucket(bucket_name):
      return DeleteCount
    bucket = self.get_bucket(bucket_name)
    for key in remove_files:
      bucket.delete_key(key)
      DeleteCount += 1
    return DeleteCount


  def clean_bucket(self, bucket_name, prefix = None):
    '''Delete all files in a bucket
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    :rtype  integer
    :return number of files that are successfully deleted from S3 bucket
    '''
    DeleteCount = 0
    if not self.is_bucket(bucket_name):
      return DeleteCount
    bucket = self.get_bucket(bucket_name)
    remove_files = self.get_all_files_in_bucket(bucket_name, prefix)
    return self.remove_files_in_bucekt(bucket_name, remove_files)

  def create_a_bucket(self, bucket_name):
    '''Create a bucket
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    '''
    if self.is_bucket(bucket_name):
      return
    s3 = self.connect()
    if self.security_token:
      s3.create_bucket(bucket_name, headers={'x-amz-security-token': self.security_token})
    else:
      s3.create_bucket(bucket_name, headers=None)
    return

  def upload_file_to_s3(self, bucket_name, local_file, remote_file, overwrite=False):
    '''Upload single file from local environment to S3
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  local_file,  string
    @param local_file,  filename on current computer
    @type  RemoteFile, string
    @param RemoteFile, specified file name that will upload onto S3 bucket
    @type  overwrite, boolean
    @param overwrite, option to overwrite the file that has same name on S3.  Default is False
    :rtype  boolean
    @param  True if upload is successful
    '''
    self.create_a_bucket(bucket_name)
    bucket = self.get_bucket(bucket_name)
    if not os.path.isfile(local_file):
      print "can not find", local_file
      return False
    file_existed = self.is_file_on_s3(bucket_name, remote_file)
    if (file_existed) and (not overwrite):
      return True
    k = Key(bucket)
    k.key = remote_file
    sys.stdout.flush()
    k.set_contents_from_filename(local_file)
#     k.set_acl('public-read')
    return True

  def upload_folder_to_s3(self, bucket_name, local_folders = [], prefix = None, overwrite = False):
    '''Attempts to upload the entire folder onto S3.  Note that the folder hierarchy is not maintained, all the files uploaded onto S3 will have signature like:
       S3://bucket_name/prefix/os.path.basename(local_folders[0])/file1
       S3://bucket_name/prefix/os.path.basename(local_folders[0])/file1
       ...
       S3://bucket_name/prefix/os.path.basename(local_folders[1])/file1
       S3://bucket_name/prefix/os.path.basename(local_folders[1])/file2
       ...
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  local_folders: list of string
    @param local_folders: list of folders that you want to upload onto S3
    @type  prefix, string
    @param prefix, a particular prefix added onto the signature of the folder on S3 bucket. E.g. if you call the method with prefix='/foo/', 
                   the all files will have a prefix signature like
                   S3://bucket_name/foo/os.path.basename(local_folders[0])/file1
                   S3://bucket_name/foo/os.path.basename(local_folders[1])/file1
    @type  overwrite, boolean
    @param overwrite, option to choose overwrite same files that are on the bucket already
    @type  security_token:  string
    @param security_token:  AWS temporary security token
    :rtype  float
    @param  the overall size (in MB) uploaded onto S3
    '''
    self.create_a_bucket(bucket_name)
    bucket = self.get_bucket(bucket_name)
#     existed_files = self.get_all_files_in_bucket(bucket_name)
    # upload
    toFileErrorCount = 0
    toFileTransferCount = 0
    TotalSize = 0.0 # MB
    for folder in local_folders:
      basename = os.path.basename(folder)
      for local_file in glob.glob(folder+"/*.*"):
        if not os.path.isfile(local_file):
          print "local file", local_file, "does NOT exist. skipping..."
          toFileErrorCount += 1
          continue
        remote_file = basename + "/%s" % os.path.split(local_file)[1]
        if prefix:
  #         remote_file = os.path.join(prefix, remote_file)
          remote_file = "%s/%s" % (prefix, remote_file)
        if self.is_file_on_s3(bucket_name, remote_file) and (not overwrite):
          continue
        print "upload", os.path.basename(local_file), "(", (float)(os.stat(local_file).st_size)/(1024.0*1024.0), " MB)..."
        sys.stdout.flush()
        TotalSize += (float)(os.stat(local_file).st_size)/(1024.0*1024.0)
        k = Key(bucket)
        k.key = remote_file
        k.set_contents_from_filename(local_file)
#         k.set_acl('public-read')
        print "Copied TO S3: %s" % remote_file
        toFileTransferCount += 1
    print "*" * 50
    if toFileErrorCount:
      print "Encounter", toFileErrorCount, "failure transferring files to", bucket_name
    print "Uploaded", toFileTransferCount, "files to bucket", bucket_name
    print "*" * 50
    return TotalSize

  def download_file_from_s3(self, bucket_name, remote_file, local_file, overwrite = False):
    '''Download a single file from S3
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  remote_file, string
    @param remote_file, files that will be download from S3, e.g. foo/root_folder/sub_folder/file1
    @type  local_file,  string
    @param local_file,  filename assigned for local computer, e.g. /home/user/data/file1
    :rtype  boolean
    :return True if download is successful
    '''
    if local_file[-1] == '/' or os.path.isdir(local_file):
      print "INFO: download_file_from_s3: Skipping download of the contents of S3://%s/%s to file since local target, '%s', is a directory name rather than a filename." % (bucket_name, remote_file, local_file)
      return True;
    if os.path.isfile(local_file) and (not overwrite):
      return True
    if remote_file[-1] == '/':
      print "INFO: download_file_from_s3: Skipping download of the contents of S3://%s/%s to file since remote object, '%s', is a directory key rather than a file key." % (bucket_name, remote_file, local_file)
      return True;
    if not self.is_bucket(bucket_name):
      return False
    bucket = self.get_bucket(bucket_name)
    k = Key(bucket)
    k.key = remote_file
    try:
      k.get_contents_to_filename(local_file)
    except boto.exception.S3ResponseError, err:
      print "failed to find file", remote_file, "in bucket", bucket_name
      print "Exception:", err
      gc.collect()
      return False
    gc.collect()
    return True

  def download_folder_from_s3(self, bucket_name, remote_folder, out_folder, overwrite=False):
    '''Download a folder from given S3 bucket and put it into given output folder.  Note that there is no folder concept in S3, only signatures.
       The folder here means the key name splitted by slash '/'.  For example, if you specify remote_folder as '/foo/key1/folder1/', it will download all files that have prefix
       '/foo/key1/folder1/' from S3 bucket
  
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  remote_folder, string
    @param remote_folder, prefix of the files that reside on S3
    @type  out_folder, string
    @param out_folder, output folder to store the downloaded files on local computer, e.g. /home/user/data/output_folder/
    @type  overwrite, boolean
    @param overwrite, option to overwrite the files that share same name on client computer
    :rtype  [integer, integer]
    :return [number of files downloaded, number of failure encounter]
    '''
    fromFilesTransferCount = 0
    fromFilesErrorCount = 0
    if not self.is_bucket(bucket_name):
      return fromFilesTransferCount, fromFilesErrorCount
    bucket = self.get_bucket(bucket_name)
    print remote_folder
    rs = bucket.list(remote_folder)
    file_list = []
    for key in rs:
      file_list.append(str(key.name))
    print file_list
    # randomize the file list to avoid conflict when multiple EC2s try to download them at the same time
    rand_order = range(0, len(file_list))
    random.shuffle(rand_order)
    rand_list = []
    for ii in range(0, len(rand_order)):
      rand_list.append(file_list[rand_order[ii]])
    # start to download
    for remote_file in rand_list:
      remote_dir, filename = os.path.split(remote_file)
      if remote_dir:
        local_dir = os.path.join(out_folder, remote_dir)
      else:
        local_dir = os.path.join(out_folder, "")
      if not os.path.exists(local_dir):
        os.makedirs(local_dir)
      local_file = os.path.join(local_dir, filename)
      if os.path.isfile(local_file) and (not overwrite):
        continue
      k = Key(bucket)
      k.key = remote_file
      try:
        k.get_contents_to_filename(local_file)
      except boto.exception.S3ResponseError, err:
        print "download", remote_file, "failed, exception cod:", err
        fromFilesErrorCount += 1
      except IOError, err:
        print "IOError raised as trying to get file", remote_file, "to", local_file
        fromFilesErrorCount += 1
      else:
#         k.set_acl('public-read')
  #       print "download", remote_file, "to", local_file, "..."
        fromFilesTransferCount += 1
    print "*" * 50
    if fromFilesErrorCount:
      print "Encountered", fromFilesErrorCount, "errors as downloading from", bucket_name
    print "Download", fromFilesTransferCount, " files from", remote_folder, "in bucket", bucket_name
    print "*" * 50
    return fromFilesTransferCount, fromFilesErrorCount

  def download_all_files_from_bucket(self, bucket_name, out_folder, prefix = None, overwrite=False):
    '''Download all files from given s3 bucket to the OutFolder. Note that the file hierarchy in the bucket is reserved.  For example
       file 'S3://bucket_name/foo/key/folder/file1' will be downloaded to out_folder/foo/key/folder/file1
  
    @type  bucket_name, string
    @param bucket_name, S3 bucket name
    @type  remote_folder, string
    @param remote_folder, prefix of the files that reside on S3
    @type  out_folder, string
    @param out_folder, output folder to store the downloaded files on local computer, e.g. /home/user/data/output_folder/
    :rtype  [integer, integer]
    :return [number of files downloaded, number of failure encounter]
    '''
    fromFilesTransferCount = 0
    fromFilesErrorCount = 0
    if not self.is_bucket(bucket_name):
      return fromFilesTransferCount, fromFilesErrorCount
    bucket = self.get_bucket(bucket_name)
    files_on_bucket = self.get_all_files_in_bucket(bucket_name, prefix)
    # randomize the file list to avoid conflict when multiple EC2 download the same file
    key_list_order = range(0, len(files_on_bucket))
    random.shuffle(key_list_order)
    file_list = []
    for ii in range(0, len(key_list_order)):
      file_list.append(files_on_bucket[key_list_order[ii]])
    # start to download
    for remote_file in file_list:
      remote_dir, filename = os.path.split(remote_file)
      if remote_dir:
        local_dir = out_folder + "/%s" % remote_dir
      else:
        local_dir = out_folder
      if not os.path.exists(local_dir):  os.makedirs(local_dir)
      local_file = os.path.join(local_dir, filename)
      if os.path.isfile(local_file) and (not overwrite):
        continue
      k = Key(bucket)
      k.key = remote_file
      try:
        k.get_contents_to_filename(local_file)
      except boto.exception.S3ResponseError, err:
        print "download", remote_file, "failed, exception code:", err
        fromFilesErrorCount += 1
      except IOError, err:
        print "IOError raised as trying to get file", remote_file, "to", local_file
        fromFilesErrorCount += 1
      else:
#         k.set_acl('public-read')
        print "Download", remote_file, "to", local_file, "..."
        fromFilesTransferCount += 1
    
    print "*" * 50
    if fromFilesErrorCount:
      print "Encountered", fromFilesErrorCount, "errors as downloading from", bucket_name
    print "Download", fromFilesTransferCount, "from bucket", bucket_name
    print "*" * 50
    return fromFilesTransferCount, fromFilesErrorCount

class S3_LOG(object):
  '''A simple class that uploads a log file to s3
  '''
  def __init__(self, s3_conn, bucket_name = None, local_log = None, remote_log = None):
    self.s3_conn = s3_conn
    self.bucket  = bucket_name
    self.local_log = local_log
    self.remote_log = remote_log
  def upload(self):
    self.s3_conn.upload_file_to_s3(self.bucket, self.local_log, self.remote_log, overwrite = True)
