FROM python:2.7.12
MAINTAINER Andy Neff <andrew.neff@visionsystemsinc.com>

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends unzip curl ca-certificates && \
    cd /usr/bin && \
    curl -LO https://github.com/ninja-build/ninja/releases/download/v1.7.1/ninja-linux.zip && \
    unzip ninja-linux.zip && \
    rm ninja-linux.zip && \
    DEBIAN_FRONTEND=noninteractive apt-get purge -y --auto-remove unzip curl ca-certificates && \
    rm -rf /var/lib/apt/lists/*

ENV BOXM2_OPENCL_DIR=/vxl/share/vxl/cl/boxm2/ \
    VOLM_DIR=/vxl/share/vxl \
    PYTHONPATH=${PYTHONPATH:+${PYTHONPATH}:}/vxl/lib/python2.7/site-packages/vxl/

ARG BUILD_TYPE=Release
#Build VXL
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        cmake python python-dev gcc git g++ bzip2 ca-certificates && \
    git clone -b public_2016_07_19 https://github.com/andyneff/vxl.git /vxl_src && \
    mkdir /vxl_src/build && \
    cd /vxl_src/build && \
    cmake -G Ninja /vxl_src -DBUILD_BRL_PYTHON=1 \
          -DCMAKE_BUILD_TYPE=${BUILD_TYPE} && \
    ninja -j $(nproc) && \
    DEBIAN_FRONTEND=noninteractive apt-get purge -y --auto-remove git gcc g++ python-dev cmake ca-certificates && \
    rm -rf /var/lib/apt/lists/* && \
#Copy over VXL files aka INSTALL
    mkdir -p /vxl/lib/python2.7/site-packages/vxl && \
    for d in $(find /vxl_src/ -type d -name pyscripts | sed 's|$|/*|' | grep -v core); do \
      cp -r ${d} /vxl/lib/python2.7/site-packages/vxl/; \
    done && \
    cp /vxl_src/contrib/brl/bpro/core/pyscripts/* /vxl/lib/python2.7/site-packages/vxl/ && \
    mv /vxl_src/build/lib/*.so /vxl/lib/python2.7/site-packages/vxl/ && \
    mkdir -p /vxl/share/vxl/cl && \
    mv /vxl_src/contrib/brl/bseg/boxm2/ocl/cl /vxl/share/vxl/cl/boxm2 && \
    mv /vxl_src/contrib/brl/bseg/boxm2/reg/ocl/cl /vxl/share/vxl/cl/reg && \
    mv /vxl_src/contrib/brl/bseg/boxm2/vecf/ocl/cl /vxl/share/vxl/cl/vecf && \
    mv /vxl_src/contrib/brl/bseg/boxm2/volm/cl /vxl/share/vxl/cl/volm && \
    mv /vxl_src/contrib/brl/bseg/bstm/ocl/cl /vxl/share/vxl/cl/bstm && \
    mv /vxl_src/contrib/brl/bbas/volm/*_*.txt /vxl/share/vxl && \
    #mv /vxl_src/build/bin /vxl_install/bin && \
    #mv /vxl_src/build/lib/*.a /vxl_install/lib/ && \
#Remove VXL SRC and build
    rm -rf /vxl_src /vxl_src/build

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        libgeos-c1 ca-certificates gcc python-gdal && \
    pip install gbdxtools==0.5.4 gbdx-auth==0.2.0 shapely==1.5.17 boto==2.42.0 && \
    DEBIAN_FRONTEND=noninteractive apt-get purge -y --auto-remove gcc && \
    rm -rf /var/lib/apt/lists/* /root/.cache/pip

LABEL dustify.runargs="-v %PWD%/catalog_work:/mnt/work"


