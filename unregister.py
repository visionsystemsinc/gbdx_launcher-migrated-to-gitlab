#!/usr/bin/env python

import gbdxtools
import register
import logging

def unregister_task(gbdx, task_name):
  own_tasks = []
  for own_task_name, own_task_json in register.tasks.iteritems():
    own_tasks.append("%s:%s" % (own_task_name, own_task_json['version']))

  if task_name not in own_tasks:
    raise Exception("This is not one of our tasks... you better not remove "
                    "someone else's tasks, they may get upset")
  
  print 'Unregistering', task_name
  gbdx.gbdx_connection.delete('https://geobigdata.io/workflows/v1/tasks/%s' % (task_name))
  print 'Unregistered', task_name


def main(args=None):
  args = register.parser().parse_args(args)
  if args.verbose:
    try: # for Python 3
      from http.client import HTTPConnection
    except ImportError:
      from httplib import HTTPConnection

    HTTPConnection.debuglevel = 1

    logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

  gbdx = gbdxtools.Interface()
  task_list = gbdx.task_registry.list()
  for task_name, task_json in register.tasks.iteritems():
    task_name_with_version = "%s:%s" % (task_name, task_json['version'])
    if task_name_with_version in task_list:
      unregister_task(gbdx, task_name_with_version)
    else:
      print task_name, 'is not registered'



if __name__=="__main__":
  main()
