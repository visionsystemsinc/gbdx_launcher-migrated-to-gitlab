# GBDX Launcher #

This Docker is meant to register and launch a GBDX task. 

1. You need to create a file 
`.gbdx-config` containing the following (as described [here](https://github.com/tdg-platform/gbdx-auth#ini-file)):

```
[gbdx]
auth_url = https://geobigdata.io/auth/v1/oauth/token/
client_id = your_client_id
client_secret = your_client_secret
user_name = your_user_name
user_password = your_password
```

**Make sure this file has read-write permissions**

2. Build the docker
3. ```docker run -it --rm -v `pwd`/.gbdx-config:/root/.gbdx-config {IMAGENAME}```