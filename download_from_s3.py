#!/usr/bin/env python
'''
Created on Sep 29, 2016

@author: yi
'''
import gbdxtools
import os
import s3_helper

### Input
s3_out_prefix = "malaysian/3DModel-Apr24_1/DSM"
out_folder = '/home/yi/mnt/fileserver/projects/GBDX/malaysian/DSM'
if not os.path.exists(out_folder):
  os.makedirs(out_folder)

### Start to connect GBDX
gbdx = gbdxtools.Interface()
url = 'https://geobigdata.io/s3creds/v1/prefix?duration=129600'
r = gbdx.s3.gbdx_connection.get(url)
r.raise_for_status()
s3_info = r.json()
s3_access_key = s3_info["S3_access_key"]
s3_secret_key = s3_info["S3_secret_key"]
s3_security_token = s3_info["S3_session_token"]
s3_bucket = s3_info["bucket"]
s3_prefix_root = s3_info["prefix"]
 
# check whether files are on S3
s3_conn = s3_helper.S3(aws_access_key_id=s3_access_key, aws_secret_access_key=s3_secret_key, security_token=s3_security_token)
s3_prefix = "%s/%s" % (s3_prefix_root, s3_out_prefix)

print s3_prefix
print s3_bucket
print s3_prefix_root
remote_files = s3_conn.get_all_files_in_bucket(s3_bucket, s3_prefix)

for remote_file in remote_files:
  remote_folder = os.path.dirname(remote_file.replace("%s/" % s3_prefix, ""))
  local_folder = os.path.join(out_folder, remote_folder)
  if not os.path.exists(local_folder):
    os.makedirs(local_folder)
  local_file = os.path.join(local_folder, os.path.basename(remote_file))
  if os.path.isfile(local_file):
    continue
  print 'download {}...'.format(remote_file)
  s3_conn.download_file_from_s3(s3_bucket, remote_file, local_file)

